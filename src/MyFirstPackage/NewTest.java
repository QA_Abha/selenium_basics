package MyFirstPackage;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class NewTest {
	/*
	 * TestCase to verify the alert when wrong Username password are inserted into linked in
	 * Use of textbox,button 
	 *Asserts and conditions 
	 */
	@Test
	public void LinkedInLoginVerification() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\abhas\\eclipse-workspace\\chromedriver_win32\\chromedriver.exe");
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		String baseWebUrl = "https://www.linkedin.com/";
		String expectedWebsiteTitle = "LinkedIn: Log In or Sign Up";
		String actualWebsiteTitle = "";
		/*Launch Firefox browser and browse the Base URL*/
		driver.get(baseWebUrl);
		/* get the actual value of the title*/

		actualWebsiteTitle = driver.getTitle();

		/*
		 * Compare the Website actual title against the expected title
		 * If both titles matches then result is "Passed" else "Failed"
		 */
		//
		if (actualWebsiteTitle.contentEquals(expectedWebsiteTitle)){
			System.out.println("Test Passed!");
		} else {
			System.out.println("Test Failed!");
		}

		//click on sign in Button
		WebElement SigninBtn= driver.findElement((By.xpath("//a[@class='nav__button-secondary']")));
		SigninBtn.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WebElement username = driver.findElement(By.name("session_key"));
		username.click();
		username.sendKeys("test@gmail.com");
		WebElement password = driver.findElement((By.name("session_password")));
		password.sendKeys("test@1");
		WebElement signIn= driver.findElement((By.xpath("//button[@class='btn__primary--large from__button--floating']")));
		signIn.click();
		WebElement errorMsg= driver.findElement((By.xpath("//div[@id='error-for-password']")));
		String errMsgTitle="Hmm, that's not the right password. Please try again or ";
		//Verify the error msg
		if (errorMsg.getText().contains(errMsgTitle)){
			System.out.println("error msg verified");
		} else {
			System.out.println("wrong error msg");
		}
		//Close the browser
		driver.close();
		/*Exiting the System*/
		System.exit(0);

	}

	/*
	 *Testcase to Click on any Item in Amazon and add to cart 
	 *Code to handle windows switching and accessing elements
	 *Use of text box . 
	 * */

	@Test
	public void AmazonSearchWithWindowsSwitch() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\abhas\\eclipse-workspace\\chromedriver_win32\\chromedriver.exe");
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		String baseWebUrl = "https://www.amazon.in/";
		driver.get(baseWebUrl);
		WebElement tabSearchBox = driver.findElement(By.id("twotabsearchtextbox")); 
		//Search for books
		tabSearchBox.sendKeys("books");
		//press Enter button
		tabSearchBox.sendKeys(Keys.RETURN);
		//Verify that correct URL is hit 
		String expectedURL = "https://www.amazon.in/s?k=books&ref=nb_sb_noss_1";
		if (driver.getTitle().contentEquals(expectedURL)){
			System.out.println("Books URL hit");
		} else {
			System.out.println("Wrong URL");
		}

		//Click on Book 
		WebElement desiredBook =  driver.findElement(By.xpath("//span[contains(text(),'How to Stop Worrying and Start Living: Time-Tested')]"));
		desiredBook.click();
		//Wait for add to cart button 
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {}
		// Get current window handle
		String parentWinHandle = driver.getWindowHandle();
		// Get the window handles of all open windows
		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for(String handle: winHandles){
			if(!handle.equals(parentWinHandle)){
				driver.switchTo().window(handle);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				System.out.println("Title of the new window: " +
						driver.getTitle());
				System.out.println("Closing the new window...");
				//Click on the button in new window
				WebElement addtoCart = driver.findElement(By.id("add-to-cart-button"));
				addtoCart.click();
				driver.close();
			}
		}

	}
	/*
	 * Testcase to demostrate the use of dropdown and 
	 * different functions avaliable with dropdowns 
	 * with example.
	 * */
	@Test
	public void DropDownUse() {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\abhas\\eclipse-workspace\\chromedriver_win32\\chromedriver.exe");
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		String baseWebUrl = "https://www.amazon.in/";
		driver.get(baseWebUrl);
		WebElement testDropDown = driver.findElement(By.id("searchDropdownBox"));  
		Select dropdown = new Select(testDropDown);  
		//Index is zero based
		//Select by index
		dropdown.selectByIndex(1);
		//Select by value 
		dropdown.selectByValue("search-alias=baby");
		//Select by visible text 
		dropdown.selectByVisibleText("Books");
		//Below commented code wont work as you can only deslect options of a multiselect
		//But the syntax to deselect will work.
		//dropdown.deselectByVisibleText("Books");
		//Select by index
		dropdown.selectByIndex(6);
		driver.close();

	}
	
	/*
	 * RadioButtonUsage example before checking we should check if the button is already selected
	 * and then select /unselect according to requirement.
	 * 
	 * */
	 
	@Test	
	public void RadioButtonUsage() {
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\abhas\\eclipse-workspace\\chromedriver_win32\\chromedriver.exe");
	// TODO Auto-generated method stub
	WebDriver driver = new ChromeDriver();
	String baseWebUrl = "http://demo.guru99.com/test/radio.html";
	driver.get(baseWebUrl);
	WebElement radioBtn = driver.findElement(By.id("vfb-7-1"));
	
	//check if the radio button is selected 
	if (radioBtn.isSelected()) {
		System.out.println("Radio btn is toggled on dont select again");
	}
	else {
		System.out.println("Radio btn is toggled off ..select it");
		radioBtn.click();}
	
	
	
}
	
	/*
	 * CheckBoxUsage example before checking we should check if the button is already selected
	 * and then select /unselect according to requirement.
	 * 
	 * */
	 
	@Test	
	public void CheckBoxUsage() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\abhas\\eclipse-workspace\\chromedriver_win32\\chromedriver.exe");
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		  //Selecting Checkbox and using isSelected Method		
        driver.get("http://demo.guru99.com/test/facebook.html");					
        WebElement chkFBPersist = driver.findElement(By.id("persist_box"));							
        for (int i=0; i<2; i++) {											
            chkFBPersist.click (); 			
            System.out.println("Facebook Persists Checkbox Status is -  "+chkFBPersist.isSelected());							
        }		
		driver.close();		
        		
    }		

	
	
	
}

