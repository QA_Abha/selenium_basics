package MyFirstPackage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;


public class Testclass {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\abhas\\eclipse-workspace\\chromedriver_win32\\chromedriver.exe");
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		String baseWebUrl = "https://www.linkedin.com/";
		String expectedWebsiteTitle = "LinkedIn: Log In or Sign Up";
		String actualWebsiteTitle = "";
		/*Launch Firefox browser and browse the Base URL*/
		driver.get(baseWebUrl);
		/* get the actual value of the title*/
		
		actualWebsiteTitle = driver.getTitle();
		
		/*
		 * Compare the Website actual title against the expected title
		 * If both titles matches then result is "Passed" else "Failed"
		*/
		//
		 if (actualWebsiteTitle.contentEquals(expectedWebsiteTitle)){
			 System.out.println("Test Passed!");
			 } else {
			 System.out.println("Test Failed!");
			 }
		 
		//click on sign in Button
		 WebElement SigninBtn= driver.findElement((By.xpath("//a[@class='nav__button-secondary']")));
		 SigninBtn.click();
		 try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		WebElement username = driver.findElement(By.name("session_key"));
		username.click();
		username.sendKeys("test@gmail.com");
		WebElement password = driver.findElement((By.name("session_password")));
		password.sendKeys("test@1");
		WebElement signIn= driver.findElement((By.xpath("//button[@class='btn__primary--large from__button--floating']")));
		signIn.click();
		WebElement errorMsg= driver.findElement((By.xpath("//div[@id='error-for-password']")));
		String errMsgTitle="Hmm, that's not the right password. Please try again or ";
		//Verify the error msg
		 if (errorMsg.getText().contains(errMsgTitle)){
			 System.out.println("error msg verified");
			 } else {
			 System.out.println("wrong error msg");
			 }
		 
		
		
		//verify that page open is correct
		 /*closing Firefox Browser*/
	//	driver.close();
		 /*Exiting the System*/
	//	 System.exit(0);





	}

}
